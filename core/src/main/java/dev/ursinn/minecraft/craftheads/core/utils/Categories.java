package dev.ursinn.minecraft.craftheads.core.utils;

import org.json.simple.JSONArray;

public interface Categories {

    void loadFiles();

    JSONArray getHeadCategories();
}
